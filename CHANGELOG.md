# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 2020-11-24

### Added

- Add user email from git config to git plugin

### Changed

- Replaced [nodegit](https://www.npmjs.com/package/nodegit) with
  [simple-git](https://www.npmjs.com/package/simple-git) as nodegit causing
  too many issues (builds only available for the latest version and latest
  versions of node

## [1.0.1] - 2019-11-12

### Fixed

- Fixed saving on Ctrl+d

### Added

- `writePromise()` for ensuring all writes are completed

### Changed

- Ensure write of tickings is sequential
- Ensure tickings are parsed correctly

## [1.0.0] - 2019-11-11

### Changed

- Renamed `pause()` in API to `toggle()`

## [0.9.2] - 2019-04-04

### Changed

- Cleaned files included in npm bundle

## [0.9.1] - 2019-03-24

### Changed

- Updated nodegit

## [0.9.0] - 2018-05-11

Initial version with the git plugin working

[Unreleased]: https://gitlab.com/bytes.nz/keeps-on-ticking/compare/v1.1.0...HEAD
[1.1.0]: https://gitlab.com/bytes.nz/keeps-on-ticking/compare/v1.0.1...v1.1.0
[1.0.1]: https://gitlab.com/bytes.nz/keeps-on-ticking/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/bytes.nz/keeps-on-ticking/compare/v0.9.2...v1.0.0
[0.9.2]: https://gitlab.com/bytes.nz/keeps-on-ticking/compare/v0.9.1...v0.9.2
[0.9.1]: https://gitlab.com/bytes.nz/keeps-on-ticking/compare/v0.9.0...v0.9.1
[0.9.0]: https://gitlab.com/bytes.nz/keeps-on-ticking/-/tags/v0.9.0
