export interface Timer {
  /**
   * Start the timer if it isn't already
   *
   * @returns Whether the timer needed to be started
   */
  start (): boolean;

  /**
   * Stop the timer if it isn't already
   *
   * @returns The number of seconds in last ticking or false if the timer was
   *   already stopped
   */
  stop (time?: Date): false | number;

  /**
   * Start/stop timing
   *
   * @returns The number of seconds in last ticking or false if the timer was
   *   already stopped
   *
   */
  toggle (time?: Date): false | number;

  /**
   * Stop and stop the timer, creating a new timing segment
   *
   * @returns The number of seconds in last ticking or false if the timer was
   *   already stopped
   */
  newSegment (time?: Date): false | number;

  /**
   * Return the current ticking talley
   *
   * @returns Ticking talley in number of seconds
   */
  total (): number;

  /**
   * Reset the current ticking talley
   */
  reset (): void;
}
