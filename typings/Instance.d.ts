import { PartialTicking, Ticking } from './Tickings';
import { Storage } from './Storage';
import { Timer } from './Timer';

export interface Instance {
  /// Instance timer
  timer: Timer;

  /// Instance storage
  storage: Storage;

  /**
   * Send a message to the user
   *
   * @param message Message to send
   *
   * @returns A Promise that resolves when the message has been sent
   */
  message: (message: string) => void;

  /**
   * Create a Ticking object
   *
   * @param ticking Partial Ticking object to base the created Ticking object
   *   on
   *
   * @returns A Ticking object
   */
  createTicking: (ticking: PartialTicking) => Ticking;
}
