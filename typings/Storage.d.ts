import { Ticking } from './Tickings';

/**
 * Interface for Tickings Storage modules to implement
 */
export interface Storage {
  /**
   * Get the current list of tickings
   */
  getTickings: () => Promise<Array<Ticking>>;

  /**
   * Store a new ticking
   */
  storeTicking: (ticking: Ticking) => Promise<void>;

  /**
   * Get a Promise that will resolve once any write operations that are
   * currently underway are completed
   */
  writePromise: () => Promise<void>;
}
