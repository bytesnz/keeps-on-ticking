import { Ticking } from './Tickings';
import { Storage } from './Storage';
import { Instance } from './Instance';

export interface Plugin {
  /// Name of string
  name: string;

  /**
   * Used by KOT to determine if it should run the plugin for the project
   * that it was started in
   */
  isOne(): Promise<boolean>;

  /**
   * Group the given tickings
   *
   * @param tickings Tickings to group
   */
  group(tickings: Array<Ticking>): { [group: string]: Ticking };

  /**
   * Creates an instance of the plugin
   *
   * @param instance KOT instance
   *
   * @returns A Promise that resolves to the plugin instance
   */
  createInstance(instance: Instance): Promise<PluginInstance>;
}

export interface PluginInstance {
  /// Name of plugin
  name: string;

  /**
   * Return the plugin string that should be included with each line of the
   * tickings file. If a non-blank string is returned, the string will be
   * prepended with `<name>:`, where <name> is the name of the plugin
   *
   * @returns A string to include in the current tickings file line, or ''
   *   to not include a string
   */
  logString(): string;

  /**
   * Initialise the plugin as required if `isOne()` has returned true
   */
  start?(): Promise<void>;

  /**
   * Actions that the plugin has. Called by given the action option to KOT
   */
  actions?: {
    /**
     * Action function. When the action is given as an option to KOT, the
     * action Will be run and KOT will then exit
     *
     * @param options Parsed options passed to KOT
     * @param args Arguments given to KOT
     */
    [action: string]: (storage: Storage, options: Options, args: Array<string>) => Promise<void>;
  };
}

export interface Options {
  /// Action to run
  action?: string;
  /// Directory to use a working directory instead of the current directory
  directory?: string
}
