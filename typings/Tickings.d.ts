/// Ticking record
export interface PartialTicking {
  /// Date of ticking record
  date?: Date,
  /// Whether the record is a temporary record
  temp?: boolean,
  /// Recorded time for the record
  time?: number,
  /// Log of the record
  log?: string,
  /// Plugin strings for the record
  plugins?: {
    [plugin: string]: string
  }
}

export interface Ticking extends PartialTicking {
  date: Date
}
