import { Instance } from '../../typings/Instance';
import { Timer } from '../../typings/Timer';

import anyTest, { TestInterface } from 'ava';
import tk from 'timekeeper';
import sinon from 'sinon';

import createTimer from './timer';

// TODO Fix any
const test = anyTest as TestInterface<{
  mockKotInstance: any,
  instance: Timer
}>;

test.beforeEach((t) => {
  t.context.mockKotInstance = {
    timer: null,
    storage: {
      writePromise: sinon.stub().returns(Promise.resolve()),
      storeTicking: sinon.stub().returns(Promise.resolve()),
      getTickings: sinon.stub().returns(Promise.resolve([]))
    },
    createTicking: sinon.stub().returnsArg(0),
    message: () => {}
  };
  t.context.instance = createTimer(t.context.mockKotInstance);
});

test('stop() returns false if already stopped', (t) => {
  t.is(false as number | false, t.context.instance.stop());
});

test('start() does not reset the time if called twice', (t) => {
  const start = new Date('2018-01-01T00:00:00');
  const end = new Date('2018-01-01T00:00:11');
  tk.freeze(start);
  t.context.instance.start();
  tk.freeze(end);
  t.context.instance.start();
  t.is(11, t.context.instance.stop());

  t.is(11, t.context.instance.total());
});

test('timer does not count tickings that are equal to or less than 10 seconds', (t) => {
  const start = new Date('2018-01-01T00:00:00');
  const end = new Date('2018-01-01T00:00:10');
  tk.freeze(start);
  t.context.instance.start();
  tk.freeze(end);
  t.is(10, t.context.instance.stop());

  t.is(0, t.context.instance.total());
  t.is(0, t.context.mockKotInstance.storage.storeTicking.callCount);
});

test('timer does count tickings that are greater than 10 seconds', (t) => {
  const start = new Date('2018-01-01T00:00:00');
  const end = new Date('2018-01-01T00:00:11');
  tk.freeze(start);
  t.context.instance.start();
  tk.freeze(end);
  t.is(11, t.context.instance.stop());

  t.is(11, t.context.instance.total());
  t.is(1, t.context.mockKotInstance.createTicking.callCount);
  t.is(1, t.context.mockKotInstance.storage.storeTicking.callCount);
  t.is(11, t.context.mockKotInstance.storage.storeTicking.getCall(0).args[0].time);
  t.is(start.getTime(), t.context.mockKotInstance.storage.storeTicking.getCall(0).args[0].date.getTime());
});

test('toggle() stops and starts the timer', (t) => {
  const start = new Date('2018-01-01T00:00:00');
  const pause = new Date('2018-01-01T00:00:11');
  const resume = new Date('2018-01-01T00:00:20');
  const end = new Date('2018-01-01T00:00:31');
  tk.freeze(start);
  t.context.instance.start();
  tk.freeze(pause);
  t.is(11, t.context.instance.toggle());

  t.is(11, t.context.instance.total());

  tk.freeze(resume);
  t.is(false as number | false, t.context.instance.toggle());
  tk.freeze(end);
  t.context.instance.stop();

  t.is(22, t.context.instance.total());
});

test('reset() resets the tickings total', (t) => {
  const start = new Date('2018-01-01T00:00:00');
  const end = new Date('2018-01-01T00:00:11');
  tk.freeze(start);
  t.context.instance.start();
  tk.freeze(end);
  t.context.instance.stop();

  t.is(11, t.context.instance.total());

  t.context.instance.reset()

  t.is(0, t.context.instance.total());
});
