import { Ticking } from '../../typings/Tickings';
import { Storage } from '../../typings/Storage';

import fs from 'fs';
import readline from 'readline';
import util from 'util';

import * as utils from './utils';

const access = util.promisify(fs.access);
const appendFile = util.promisify(fs.appendFile);

export const fileStorage = async (tickingFile: string): Promise<Storage> => {
  let writePromise;

  return {
    getTickings: () =>
        new Promise(async (resolve, reject) => {
      // Check tickings file exists first
      try {
        await access(tickingFile, fs.constants.R_OK);
      } catch (error) {
        reject(error);
        return;
      }

      const tickings: Array<Ticking> = [];
      let lineCount = 0;

      const rl = readline.createInterface({
        input: fs.createReadStream(tickingFile)
      });

      rl.on('line', (line) => {
        lineCount++;

        if (!line) {
          return;
        }

        const parts = line.split('\t');

        const ticking: Ticking = {
          date: null,
          plugins: {}
        };

        // Parse date
        const date = parts.shift();
        if (date.startsWith('*')) {
          ticking.temp = true;

          ticking.date = new Date(date.slice(1));
        } else {
          ticking.date = new Date(date);
        }

        if (isNaN(ticking.date.valueOf())) {
          console.error(`Line ${lineCount} of ${tickingFile} has an invalid date`);
          return;
        }

        // Parse value (log or time)
        const last = parts.pop();

        const timeParts = last.match(/^(\d+):(\d{2}):(\d{2})$/);

        if (timeParts) {
          ticking.time = (parseInt(timeParts[1]) * 60 * 60) + (parseInt(timeParts[2]) * 60) + parseInt(timeParts[3]);
        } else {
          ticking.log = last;
        }

        // Parse plugins
        parts.forEach((part) => {
          const bits = part.split(':', 2);

          if (bits.length === 2) {
            ticking.plugins[bits[0]] = bits[1];
          }
        });

        tickings.push(ticking);
      });

      rl.on('close', () => {
        resolve(tickings);
      });
    }),

    storeTicking: (ticking: Ticking) => {
      // Render line
      let line = ticking.date.toISOString();

      if (ticking.plugins) {
        Object.keys(ticking.plugins).forEach((plugin) => {
          line += `\t${plugin}:${ticking.plugins[plugin]}`;
        });
      }

      if (ticking.time) {
        line += `\t${utils.formatTimeLength(ticking.time)}`;
      } else if (ticking.log) {
        line += `\t${ticking.log.replace(/\t/g, '  ')}`;
      }

      let thisWritePromise;
      if (writePromise) {
        thisWritePromise = writePromise.then(() => appendFile(tickingFile, `${line}\n`));
      } else {
        thisWritePromise = appendFile(tickingFile, `${line}\n`);
      }

      thisWritePromise = thisWritePromise.then(() => {
        if (thisWritePromise === writePromise) {
          writePromise = null;
        }
      });

      writePromise = thisWritePromise;

      return writePromise;
    },

    writePromise: () => {
      return writePromise;
    }
  };
};
export default fileStorage;
