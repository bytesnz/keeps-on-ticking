import test from 'ava';

import * as utils from './utils';

test('formatTimeLength() formats time as it should', (t) => {
  const hours = 10;
  const mins = 17;
  const secs = 23;

  const seconds = (hours * 60 * 60) + (mins * 60) + secs;

  t.is(`${hours}:${mins}:${secs}`, utils.formatTimeLength(seconds));
});

test('formatTimeLength() adds a 0 to mins and secs less than 10', (t) => {
  const hours = 10;
  const mins = 7;
  const secs = 3;

  const seconds = (hours * 60 * 60) + (mins * 60) + secs;

  t.is(`${hours}:0${mins}:0${secs}`, utils.formatTimeLength(seconds));
});

test('find() returns value from the interator function on a hit', (t) => {
  const testArray = [
    {
      name: 'test1',
      value: 'good'
    },
    {
      name: 'test2',
      value: 'bad'
    }
  ];

  t.is('good', utils.find(testArray, (value) => {
    if (value.name === 'test1') {
      return value.value
    }
  }));
});

test('find() returns undefined if no value is found', (t) => {
  const testArray = [
    {
      name: 'test1',
      value: 'good'
    },
    {
      name: 'test2',
      value: 'bad'
    }
  ];

  t.is('undefined', typeof utils.find(testArray, (value) => {
  }));
});
