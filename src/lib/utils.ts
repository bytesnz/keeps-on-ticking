import { Ticking } from '../../typings/Tickings';

export const minutes = 60;
export const hours = 60 * 60;

/**
 * Format a number of seconds into hh:mm:ss
 *
 * @param length Number of seconds to format
 *
 * @returns Formatted string
 */
export const formatTimeLength = (length: number): string => {
  const hoursCount = Math.floor(length / hours);
  const leftover = length % hours;
  const minutesCount = Math.floor(leftover / minutes);
  const secondsCount = Math.round(leftover % minutes);

  return `${hoursCount}:${minutesCount < 10 ? '0' : ''}${minutesCount}:${secondsCount < 10 ? '0' : ''}${secondsCount}`;
};

/**
 * Like Array.prototype.find, but returns the returned value from the interator
 * function rather than the value of the matching index
 *
 * @param array Array to scan through
 * @param fn Iterator function
 *
 * @returns The returned value if found
 */
export const find = (array: Array<any>, fn: (value: any) => any) => {
  for (let i = 0; i < array.length; i++) {
    const value = fn(array[i]);

    if (value) {
      return value;
    }
  }
};
