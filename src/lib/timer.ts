import { Storage } from '../../typings/Storage';
import { PartialTicking, Ticking } from '../../typings/Tickings';
import { Instance } from '../../typings/Instance';
import { Timer } from '../../typings/Timer';

import * as utils from './utils';

/**
 * KOT Timer functionality
 */
export const createTimer = (instance: Instance): Timer => {
  let tickingsTalley = 0;
  let segmentStartTime;
  let paused = true;

  const start = () => {
    if (paused) {
      paused = false;
      segmentStartTime = new Date();
    }
  };

  const stop = (time: Date = new Date()) => {
    if (paused) {
      return false;
    }

    const difference = (time.valueOf() - segmentStartTime.valueOf()) / 1000;

    if (difference > 10) {
      tickingsTalley += difference;

      const ticking = instance.createTicking({
        time: difference,
        date: segmentStartTime
      });

      instance.storage.storeTicking(ticking);
    }

    paused = true;

    return difference;
  };

  const toggle = (time: Date = new Date()) => {
    if (paused) {
      start();
      return false;
    } else {
      return stop(time);
    }
  };

  const newSegment = (time: Date = new Date()) => {
    const ticking = stop(time);
    start();

    return ticking;
  };

  return <Timer>{
    start,
    stop,
    toggle,
    newSegment,
    total: () => tickingsTalley,
    reset: () => { tickingsTalley = 0; }
  };
};
export default createTimer;
