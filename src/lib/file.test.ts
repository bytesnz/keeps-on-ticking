import { Storage } from '../../typings/Storage';

import anyTest, { TestInterface } from 'ava';
import util from 'util';
import { vol } from '../test/lib/unionfs';
import fs from 'fs';

import createStorage from './file';

interface Context {
  filename: string;
  storage: Storage;
};

const test = anyTest as TestInterface<Context>;

let fileNumber = 1;

const mockTickings = `
2018-04-13T15:30:05.421Z	git:master	1:0:0
2018-04-13T17:23:49.252Z	git:master	0:48:08
2018-04-15T13:03:46.074Z	git:master	Fixed up signals and exiting
2018-04-15T13:03:46.074Z	git:master	0:23:01
`;

test.beforeEach(async (t) => {
  t.context.filename = `/test${fileNumber++}`;

  vol.fromJSON({ [t.context.filename]: mockTickings });

  t.context.storage = await createStorage(t.context.filename);
});

test.skip('getTickings() parses tickings', async (t) => {
  t.deepEqual([
    {
      date: new Date('2018-04-13T15:30:05.421Z'),
      plugins: {
        git: 'master'
      },
      time: 3600
    },
    {
      date: new Date('2018-04-13T17:23:49.252Z'),
      plugins: {
        git: 'master'
      },
      time: 2888
    },
    {
      date: new Date('2018-04-15T13:03:46.074Z'),
      plugins: {
        git: 'master'
      },
      log: 'Fixed up signal and exiting'
    },
    {
      date: new Date('2018-04-15T13:03:46.074Z'),
      plugins: {
        git: 'master'
      },
      time: 13801
    }
  ], await t.context.storage.getTickings());
});
