#!/usr/bin/env node

import { Plugin, PluginInstance } from '../../typings/Plugin';
import { PartialTicking, Ticking } from '../../typings/Tickings';
import { Instance } from '../../typings/Instance';

import process from 'process';
import fs from 'fs';
import path from 'path';
import readline from 'readline';
import * as plugins from '../plugins';
import commandLineArgs from 'command-line-args';
import commandLineUsage from 'command-line-usage';
import createFileStore from '../lib/file';
import * as utils from '../lib/utils'
import createTimer from '../lib/timer';

const claOptions = [
  {
    name: 'action',
    alias: 'a',
    description: 'The plugin action to run',
    type: String
  },
  {
    name: 'directory',
    alias: 'd',
    description: 'The directory of the project to track in. Defaults to the current directory',
    type: String
  },
  {
    name: 'totals',
    alias: 't',
    description: 'Print the totals for tickings. Can be given a ISO-formatted date(time) to get the tickings since that date',
    type: String
  },
  {
    name: 'group',
    alias: 'g',
    description: 'Group values by the grouping given by a plugin',
    type: String
  },
  {
    name: 'filter',
    alias: 'f',
    description: 'Filter tickings with a given value',
    type: String
  },
  {
    name: 'help',
    alias: 'h',
    description: 'This help',
    type: Boolean
  }
];

const claHelpSections = [
  {
    header: 'Keep On Ticking',
    content: 'A simple development project time tracker'
  },
  {
    header: 'Usage',
    content: 'kot [-h,--help] [-d,--directory <dir>] [-a,--action <plugin_action>] [--totals,-t <iso_date> [-g,--group <plugin>] [-f,--filter <term>]]'
  },
  {
    header: 'Options',
    optionList: claOptions
  }
];

const options = commandLineArgs(claOptions, { partial: true });

if (options.help) {
  console.log(commandLineUsage(claHelpSections));
  process.exit(0);
}

const activePlugins: Array<PluginInstance> = [];

/**
 * Prints a message with a time to the screen
 *
 * @param message Message to print
 */
export const message = (message: string) => {
  const now = new Date();
  console.log(`${now.toLocaleTimeString()}  ${message}`);
};

/**
 * Writes a line to the tikings file
 *
 * @param line Line to write to the file
 */
export const createTicking = (ticking: PartialTicking): Ticking => {
  if (!ticking.date) {
    ticking.date = new Date();
  }

  let pluginStrings = {};

  // Add plugin log strings
  activePlugins.forEach((plugin) => {
    const pluginString = plugin.logString && plugin.logString();

    if (pluginString) {
      pluginStrings[plugin.name] = pluginString;
    }
  });

  if (Object.keys(pluginStrings).length) {
    ticking.plugins = pluginStrings;
  }

  return <Ticking>ticking;
};

const instance: Instance = {
  timer: null,
  storage: null,
  message,
  createTicking
};

// Check the plugins to see if there are any that should be run
Promise.all(Object.keys(plugins).map(async (key) => {
  const plugin: Plugin = plugins[key];
  if (await plugin.isOne()) {
    activePlugins.push(await plugin.createInstance(instance));
  }
})).then(async () => {
  if (options.directory) {
    process.chdir(options.directory);
  }

  const storage = await createFileStore(path.resolve('./.tickings'));
  instance.storage = storage;

  if (typeof options.totals !== 'undefined') {
    let tickings = (await storage.getTickings()).filter((ticking) => ticking.time);

    if (options.filter) {
      if (options.filter.indexOf(':') !== -1) {
        const filter = options.filter.split(':', 2);

        tickings = tickings.filter((ticking) => ticking.plugins
            && ticking.plugins[filter[0]] && ticking.plugins[filter[0]] === filter[1]);
      } else {
        tickings = tickings.filter((ticking) => ticking.plugins
            && Object.keys(ticking.plugins).find((plugin) => ticking.plugins[plugin] === options.filter));
      }
    }

    if (options.totals) {
      const timeParts = options.totals.match(/^(\d\d\d\d-\d?\d-\d?\d)(T(\d?\d:\d\d))?$/);

      if (!timeParts) {
        console.error('Unknown date(time) format');
        process.exit(1);
      }

      let time;

      if (timeParts[3]) {
        time = new Date(options.totals);
      } else {
        time = new Date(options.totals);
        time.setHours(23);
        time.setMinutes(59);
        time.setSeconds(59.999);
      }

      tickings = tickings.filter((ticking) => ticking.date
          && ticking.date.getTime() > time.getTime());
    }

    if (!options.group) {
      // Add up the time
      let count = 0;

      tickings.forEach((ticking) => {
        if (ticking.time) {
          count += ticking.time;
        }
      });

      console.log(`Total: ${utils.formatTimeLength(count)}`);
    } else {
      if (!plugins[options.group]) {
        console.error('Unknown plugin', options.group);
        process.exit(1);
      }

      const groupedTickings = plugins[options.group].group(tickings);

      Object.keys(groupedTickings).forEach((group) => {
        let count = 0

        groupedTickings[group].forEach((ticking) => {
          if (ticking.time) {
            count += ticking.time;
          }
        });

        console.log(`${group}: ${utils.formatTimeLength(count)}`);
      });
    }
    process.exit(0);
  }

  if (options.action) {
    // Check active plugins for the action
    const action = utils.find(activePlugins, (plugin) => plugin.actions
        && plugin.actions[options.action]);

    if (action) {
      await action(options, options._unknown);
      process.exit(0);
    } else {
      process.exit(1);
    }
  }

  const timer = createTimer(instance);
  instance.timer = timer;
  timer.start();

  const rlOptions = {
    input: process.stdin,
    output: process.stdout,
    prompt: ''
  };

  let rl = readline.createInterface(rlOptions);

  rl.on('line', (line) => {
    if (!line) {
      return
    }

    const ticking = createTicking({ log: line });

    storage.storeTicking(ticking);
  });

  rl.on('close', () => {
    timer.stop();

    console.log(`You spent ${utils.formatTimeLength(timer.total())} working. Good job`);
    const writePromise = storage.writePromise();

    if (writePromise) {
      writePromise.then(() => {
        process.exit(0);
      });
    } else {
      process.exit(0);
    }
  });

  // Check every 5 seconds for if we have sleeping/hibernating and if so start a new segment
  let lastTick = new Date();
  let interval;

  const checkLastTick = () => {
    const now = new Date();

    if (now.valueOf() > lastTick.valueOf() + 10000) {
      timer.stop(lastTick);

      clearInterval(interval);
      message('Paused after sleep');
    }

    lastTick = now;
  };

  const intervalTime = 5000;
  interval = setInterval(checkLastTick, intervalTime);

  rl.on('SIGINT', (code) => {
    if (timer.toggle() === false) {
      message('Resumed keeping track of ticks');
      lastTick = new Date();
      interval = setInterval(checkLastTick, intervalTime);
    } else {
      clearInterval(interval);
      message('Paused');
    }
  });

  console.log(`keeps-on-ticking\n`);
  console.log(`The following plugins are active: ` + activePlugins.map((plugin) => plugin.name).join(' '));
  console.log('Enter logs whenever you want. CTRL+C to pause/resume. CTRL+D to finish.\n');
  message('Starting tracking of ticks.');
}).catch((error) => {
  console.error(error);
  process.exit(1);
});
