import Plugin from '../../typings/Plugin';
import { Ticking } from '../../typings/Tickings';
import { Instance } from '../../typings/Instance';

import fs from 'fs';
import util from 'util';
import simpleGit, { SimpleGitOptions } from 'simple-git';

const stat = util.promisify(fs.stat);
const access = util.promisify(fs.access);
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const appendFile = util.promisify(fs.appendFile);
const chmod = util.promisify(fs.chmod);

const options: SimpleGitOptions = {
  baseDir: process.cwd(),
  binary: 'git',
  maxConcurrentProcesses: 6,
};
const git = simpleGit(options);

let currentBranch;
let currentUser;
let currentRepo;

const getCurrentBranch = async () => {
  const headRef = await readFile('.git/HEAD', 'utf8');

  return headRef.split('/').pop().replace(/[\n\r]/gm, '');
};

export const name = 'git';

export const isOne = async () => {
  try {
    const gitStat = await stat('.git');

    if (gitStat.isDirectory()) {
      return true;
    }
  } catch (error) {
    if (error.code !== 'ENOENT') {
      console.log('Could not get stat on `.git`', error);
    }
  }

  return false;
};

export const group = (tickings: Array<Ticking>) => {
  const groupedTickings = {};

  tickings.forEach((ticking) => {
    const branch = (ticking.plugins && ticking.plugins.git) || 'unknown';

    if (typeof groupedTickings[branch] === 'undefined') {
      groupedTickings[branch] = [ ticking ];
    } else {
      groupedTickings[branch].push(ticking);
    }
  });

  return groupedTickings;
};

export const createInstance = async (instance: Instance) => {
  currentBranch = await getCurrentBranch();
  const config = await git.listConfig();
  for (let i = config.files.length - 1; i >= 0; i--) {
    if (config.values[config.files[i]]['user.email']) {
      currentUser = config.values[config.files[i]]['user.email'];
      break;
    }
  }

  // Watch the HEAD reference for changes on branches
  fs.watchFile('.git/HEAD', async (curr, prev) => {
    const newCurrentBranch = await getCurrentBranch();

    if (newCurrentBranch !== currentBranch) {
      instance.timer.newSegment();
      instance.message(`Git: switched to '${newCurrentBranch}' branch`);
      currentBranch = newCurrentBranch;
    }
  })

  // Try and latch on to commit hooks to add comments to commit messages
  // (prepare-commit-message) and log when commits were committed
  // (post-commit)
  const hooks = {
    'prepare-commit-msg': {
      reason: 'the adding of tickings comments to the commit message',
      action: 'currentComments'
    }
  };

  await Promise.all(Object.keys(hooks).map(async (hook) => {
    const hookFilename = `.git/hooks/${hook}`;
    const hookCommand = `${process.argv[1]} -d "${process.cwd()}" -a ${hooks[hook].action} $1 $2 $3`;
    const hookScript = `# Keep on ticking to allow ${hooks[hook].reason}\n${hookCommand}\n`;

    // Check if the hook already exists
    try {
      await access(hookFilename, fs.constants.R_OK);

      // Check if file already contains hook
      const contents = await readFile(hookFilename, 'utf8');

      const commandRegex = new RegExp(`^${hookCommand.replace(/\$/g, '\\$')}$`, 'm');
      if (!contents.match(commandRegex)) {
        // Check that tha file is a shell script
        if (!contents.match(/^#!\/bin\/(bash|sh|zsh)$/m)) {
          console.error(`Can't add to ${hook} hook as not a shell script`);
          console.error(`Please add the running of the following to allow ${hooks[hook].reason}:`);
          console.error(hookScript);
          return;
        }

        // Add the hook to the file
        await appendFile(hookFilename, `\n${hookScript}`);
      }
    } catch (error) {
      if (error.code === 'ENOENT') {
        // Create the hook file
        await writeFile(hookFilename, `#!/bin/sh\n\n${hookScript}`);
        await chmod(hookFilename, 0o755);
      }
    }
  }));

  return <Plugin.PluginInstance>{
    name: 'git',
    logString: () => {
      return currentBranch + (currentUser ? ':' + currentUser : '');
    },
    actions: {
      /**
       * Prepends the comments that are related to the current branch since the
       * last commit to the given file
       */
      currentComments: async (options: Plugin.Options, args: Array<string>) => {
        if (!Array.isArray(args)) {
          args = [];
        }

        const filename = args.shift();
        const commitType = args.shift();
        const amendedCommitHash = args.shift();

        // Get time of last commit
        const lastCommit = (await git.log()).latest;
        let lastCommitDate;
        if (lastCommit) {
          lastCommitDate = new Date(lastCommit.date);
        }

        // Parse the ticking file
        const tickings = await instance.storage.getTickings();

        let logs = [];

        // Go through tickings collecting comments as we go
        tickings.reverse().find((ticking) => {
          if (lastCommitDate && ticking.date.getTime() < lastCommitDate.getTime()) {
            return true;
          }

          if (ticking.plugins && ticking.plugins.git === currentBranch && ticking.log) {
            logs.unshift(ticking.log);
          }
        });

        if (logs.length) {
          if (!filename) {
            console.log(logs.join('\n'));
          } else {
            let commitMessage = await readFile(filename, 'utf8');

            const logString = `# Comments from tickings:\n# ${logs.join('\n# ')}\n#`;
            if (commitMessage.startsWith('\n')) {
              commitMessage = `\n${logString}${commitMessage}`;
            } else {
              const messageLines = commitMessage.split('\n');

              messageLines.splice(2, 0, logString);

              commitMessage = messageLines.join('\n');
            }

            await writeFile(filename, commitMessage);
          }
        }
      }
    }
  };
}
