keeps-on-ticking
===========================
A simple development project time tracker

# Introduction
Keeps on ticking is a simple script to track the time you spend on projects.

Simply run it and when you close it, it will record the time it was opened
and how long it was open for in the `.tickings` file in the folder where you
ran it from.

```
yarn add -G keeps-on-ticking

kot
```

The tracker can be paused/resume by sending a SIGINT (`CTRL+C`) and closed by
sending a SIGHUP (`CTRL+D`) to finish and close the tracker.

If the folder contains magic that one of the plugins is aware of, such as the
included git plugin, you will also get features like automatic branch time
tracking and switching.

## Current Plugins
- **git** - Adds the current branch to the logs and tickings

# Example `.tickings` file
```
2018-04-13T15:30:05.421Z	git:master	1:0:0
2018-04-13T17:23:49.252Z	git:master	0:48:08
2018-04-15T13:03:46.074Z	git:master	Fixed up signals and exiting
2018-04-15T13:03:46.074Z	git:master	0:23:01
```
